﻿/*
* AUTHOR: Angel Navarrete Sanchez
* DATE: 2022/11/30
* DESCRIPTION:  Menu amb conjunt de funcions sobre els strings
*/
using System;

namespace Cadenes
{
    class Cadenes
    {
        public void Menu()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("                                         Exercicis Strings Angel Navarrete                                                ");
            Console.WriteLine("------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("\t 1.- SonIguals");
            Console.WriteLine("\t 2.- SonIgualsSenseDistincio");
            Console.WriteLine("\t 3.- EliminarLletres");
            Console.WriteLine("\t 4.- SustitueixCaracters");
            Console.WriteLine("\t 5.- DistanciaHamming");
            Console.WriteLine("\t 6.- Parentesis");
            Console.WriteLine("\t 7.- Palindrom");
            Console.WriteLine("\t 8.- InverteixParaules");
            Console.WriteLine("\t 9.- InverteixFrases");
            Console.WriteLine("\t 10.- HolaIAdeu");
            Console.WriteLine("\t 0.- Sortir");
            Console.WriteLine("------------------------------------------------------------------------------------------------------------------------\n");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.Write("Escull una opció: ");
            int opcio = Convert.ToInt32(Console.ReadLine());
            switch (opcio)
            {
                case 1:
                    SonIguals();
                    break;
                case 2:
                    SonIgualsSenseDistincio();
                    break;
                case 3:
                    EliminarLletres();
                    break;
                case 4:
                    SustitueixCaracters();
                    break;
                case 5:
                    DistanciaHamming();
                    break;
                case 6:
                    Parentesis();
                    break;
                case 7:
                    Palindrom();
                    break;
                case 8:
                    InverteixParaules();
                    break;
                case 9:
                    InverteixFrases();
                    break;
                case 10:
                    HolaIAdeu();
                    break;
                case 0:
                    Console.WriteLine("Adeu");
                    break;
                default:
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Opcio Incorrecta");
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
            }
            Console.ReadLine();
        }
        /*DESCRIPTION: Feu un programa que rebi per l’entrada 2 seqüències de caràcters i digui si aquestes són iguals.*/
        public void SonIguals() {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("--------------------SonIguals--------------------");
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("Introdueix una paraula: ");
            string paraula1 = Console.ReadLine();
            Console.Write("Introdueix un altre paraula: ");
            string paraula2 = Console.ReadLine();

            if (paraula1.CompareTo(paraula2) == 0)
            {
                Console.WriteLine("Son Iguals");
            }
            else
            {
                Console.WriteLine("No son iguals");
            }
        }
        /*DESCRIPTION: Feu un programa que rebi per l’entrada 2 seqüències de caràcters i digui si aquestes són iguals, sense tenir en compte majúscules i minúscules.*/
        public void SonIgualsSenseDistincio() {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("--------------------SonIgualsSenseDistincio--------------------");
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("Introdueix una paraula: ");
            string paraula1 = Console.ReadLine();
            Console.Write("Introdueix un altre paraula: ");
            string paraula2 = Console.ReadLine();

            if (paraula1.ToLower().CompareTo(paraula2.ToLower()) == 0)
            {
                Console.WriteLine("Son Iguals");
            }
            else
            {
                Console.WriteLine("No son iguals");
            }

        }

        /*DESCRIPTION: Feu un programa que rebi per l’entrada 1 String i després una serie indefinida de caràcters per anar traient de l’String, si és que el conté, fins que es rep un 0.*/
        public void EliminarLletres() {
            
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("--------------------EliminarLletres--------------------");
            Console.WriteLine("[Per acabar de introduir caracter posar un 0]");
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("Introdueix una paraula: ");
            string paraula = Console.ReadLine();
            string paraulaFinal = paraula;
            string caracter = " ";
            while (caracter != "0")
            {
                Console.Write("Introdueix un caracter: ");
                caracter = Console.ReadLine();
                if(caracter != "0")
                {
                        paraulaFinal = paraulaFinal.Replace(caracter, string.Empty);
                }
                
            }
            Console.WriteLine(paraulaFinal);
        }
        /*DESCRIPTION: Feu un programa que rebi per l’entrada 1 seqüència de caràcters i just després dos caràcters, el primer per substituir de l’String pel segon.*/
        public void SustitueixCaracters() {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("--------------------SustitueixCaracters--------------------");
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("Introdueix una paraula: ");
            string paraula = Console.ReadLine();
            Console.Write("Introdueix el caracter que vols subtituir de la paraula: ");
            string caracterParaula = Console.ReadLine();
            Console.Write("Introdueix per quin caracter: ");
            string caracterASubsituir = Console.ReadLine();

            string paraulaFinal = paraula.Replace(caracterParaula, caracterASubsituir);

            Console.WriteLine(paraulaFinal);

        }
        /*DESCRIPTION: Feu un programa que rebi per l’entrada 2 seqüències d’ADN i calculi la distància d’Hamming entre elles. Tingueu en compte que per poder realitzar aquest càlcul heu d’assegurar que les cadenes tinguin exactament la mateixa mida.*/
        public void DistanciaHamming() {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("--------------------DistanciaHamming--------------------");
            Console.ForegroundColor = ConsoleColor.White;

            Console.Write("Introdueix la primera secuencia d'ADN: ");
            string primeraCadena = Console.ReadLine();

            /*
            if (primeraCadena != "C" || primeraCadena != "T" || primeraCadena != "G" || primeraCadena != "A")
            {
                Console.Write("Cadena d'ADN incorrecta: ");
                primeraCadena = Console.ReadLine();
            }*/
            Console.Write("Introdueix la segona secuencia d'ADN: ");
            string segonaCadena = Console.ReadLine();
            int hamming = 0;

            if (primeraCadena.Length == segonaCadena.Length)
            {

                for (int i = 0; i < primeraCadena.Length; i++)
                {
                    if (primeraCadena[i] != segonaCadena[i])
                    {
                        hamming++;
                    }
                }

                Console.WriteLine("La distancia de Hamming es de: " + hamming);
            }
            else
            {
                Console.WriteLine("Entrada no valida");
            }
        }
        /* DESCRIPTION: Feu una aplicació que donada una seqüència amb només ‘(’ i ‘)’, digueu si els parèntesis tanquen correctament.*/
        public void Parentesis() {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("--------------------Parentesis--------------------");
            Console.ForegroundColor = ConsoleColor.White;

            Console.Write("Introdueix la secuencia de parentesis: ");
            string secuencia = Console.ReadLine();
            int contParentesisObert = 0;
            int contParentesisTancat = 0;
            foreach (char caracter in secuencia)
            {
                if (caracter != '(' && caracter != ')')
                {
                    Console.WriteLine("Entrada Incorrecta, Introdueix un altre vegada: ");
                    secuencia = Console.ReadLine();
                }
            }

            foreach (char caracter in secuencia)
            {
                if (caracter == '(')
                {
                    contParentesisObert++;
                }
                else if(caracter == ')') {
                    contParentesisTancat++;
                }
            }
            if(contParentesisObert == contParentesisTancat)
            {
                Console.WriteLine("Els parentesis es tanquen be");
            }else
            {
                Console.WriteLine("Els parentesis no es tanquen be");
            }


        }
        /*DESCRIPTION: Feu un programa que indiqui si una paraula és un palíndrom o no. Recordeu que una paraula és un palíndrom si es llegeix igual d’esquerra a dreta que de dreta a esquerra.*/
        public void Palindrom() {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("--------------------Palindrom--------------------");
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("Introdueix la paraula: ");
            string paraula = Console.ReadLine();
            bool palindrom = false;

            char[] paraulaAChar = paraula.ToCharArray();
            char[] paraulaInversa = paraula.ToCharArray();
            Array.Reverse(paraulaInversa);

            foreach (var lletra in paraulaAChar)
            {
                foreach (var lletraInversa in paraulaInversa)
                {
                    if (lletra == lletraInversa)
                    {
                        palindrom = true;
                    }
                    else
                    {
                        palindrom = false;
                    }
                }
            }
            if (palindrom == true)
            {
                Console.WriteLine("Es un palindrom");
            }
            else
            {
                Console.WriteLine("No es un palindrom");
            }
        }
        /*DESCRIPTION: Feu un programa que llegeixi paraules, i que escrigui cadascuna invertint l’ordre dels seus caràcters.*/
        public void InverteixParaules() {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("--------------------InverteixParaules--------------------");
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("Cuantes paraules vols introduir: ");
            int quantitat = Convert.ToInt32(Console.ReadLine());
            string paraula = string.Empty;
            string[] paraulesInvertides = new string [quantitat];
            int contador = 0;
            while (contador < quantitat)
            {
                Console.Write("Introdueix una paraula: ");
                paraula = Console.ReadLine();

                    char[] paraulaAChar = paraula.ToCharArray();
                    Array.Reverse(paraulaAChar);
                    string paraulaInvertida = string.Empty;
                    foreach (char lletra in paraulaAChar)
                    {
                        paraulaInvertida += lletra.ToString();
                    }
                    paraulesInvertides[contador] = paraulaInvertida;
                    contador++;

            }
            foreach (string paraulaInvertida in paraulesInvertides)
            {
                Console.WriteLine(paraulaInvertida);
            }

        }
        /*DESCRIPTION: Feu un programa que llegeixi un String i inverteixi l’ordre de les paraules dins del mateix.*/
        public void InverteixFrases() {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("--------------------InverteixFrases--------------------");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("Introdueix la frase: ");
            string frase = Console.ReadLine();
            string[] paraules = frase.Split(" ");
            Array.Reverse(paraules);

            foreach (string paraula in paraules)
            {

                Console.Write(paraula + " ");
            }

        }
        /*DESCRIPTION: Feu un programa que llegeixi una seqüència de lletres acabada en punt i digui si conté la successió de lletres consecutives ‘h’,‘o’, ‘l’, ‘a’ o no*/
        public void HolaIAdeu() {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("--------------------HolaIAdeu--------------------");
            Console.WriteLine("[Introdueix un \".\" per finalitzar]");
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("Introdueix la frase: ");
            string frase = Console.ReadLine();
            if (frase.ToLower().Contains("hola"))
            {
                Console.WriteLine("hola");
            }
            else
            {
                Console.WriteLine("adeu");
            }
        }

        static void Main(string[] args)
        {
            var menu = new Cadenes();
            menu.Menu();
        }
    }
}
